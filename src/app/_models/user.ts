export class User {
    id: number;
    name: string;
    address: string;
    email: string;
    birthdate: Date;
    deleted: boolean;

    constructor(
        id: number,
        name: string,
        address: string,
        email: string,
        birthdate: Date,
        ) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.birthdate = birthdate;
    }
}
