import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserListComponent } from './pages/user-list/user-list.component';
import { FormComponent } from './pages/form/form.component';


const routes: Routes = [
    { path: 'users', component: UserListComponent },
    { path: 'users/:id', component: FormComponent },
    { path: '**', redirectTo: 'users' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
