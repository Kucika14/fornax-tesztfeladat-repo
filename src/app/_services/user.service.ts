import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@model/user';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private userList: User[] = [
        {
            id: 1,
            name: 'Pisti',
            email: 'semmi@semmi.hu',
            address: 'Teszt cím',
            birthdate: new Date(),
            deleted: false,
        }
    ];



    constructor(private http: HttpClient) {
        const value = this.getLocalStorage();
        if (value) {
            this.userList = value;
        }
    }

    getUsers(): Observable<User[]> {
        return of(this.userList);
    }

    getUserById(id): User {
        return this.userList.filter(obj => obj.id == id)[0];
    }

    addUser(user: User) {
        this.userList.push(user);
        console.log('new user created with id:' + user.id + ' ', user);
        this.setUsersToLocalStorage();
    }

    getIndexById(id) {
        return this.userList.findIndex(x => x.id == id);
    }

    editUser(user: User) {
        this.userList[this.getIndexById(user.id)] = user;
        console.log('user with id:' + user.id + ' edited', user);
        this.setUsersToLocalStorage();
    }

    deleteUser(user: User) {
        this.userList[this.getIndexById(user.id)].deleted = true;
        console.log('user with id:' + user.id + ' deleted');
        this.setUsersToLocalStorage();
    }

    generateNextId(): number {
        return this.userList.length + 1;
    }


    getLocalStorage(): User[] {
        return JSON.parse(localStorage.getItem('userList'));
    }

    setUsersToLocalStorage() {
        localStorage.setItem('userList', JSON.stringify(this.userList));
    }

}
