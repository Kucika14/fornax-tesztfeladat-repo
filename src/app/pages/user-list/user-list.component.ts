import { Component, OnInit, Input } from '@angular/core';
import { User } from '@model/user';
import { UserService } from '@service/user.service';
import { MenuItem } from '@model/menu-item';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

    public users: User[];

    @Input() menuItem: MenuItem;

    constructor(private userService: UserService) {}

    ngOnInit() {
        this.reload();
    }

    delete(user) {
        this.userService.deleteUser(user);
    }

    generateNextId() {
        return this.userService.generateNextId();
    }

    reload() {
        this.userService.getUsers().subscribe(res => this.users = res);
    }

}
