import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@service/user.service';
import { User } from '@model/user';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  form: FormGroup;
  value: any;
  user: User;
  existingUser: boolean;


  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private activeRouter: ActivatedRoute,
    private router: Router,
    ) {
      if(this.userService.getUserById(this.activeRouter.params['value'].id)) {
        this.user = this.userService.getUserById(this.activeRouter.params['value'].id)
        this.existingUser = true;
      } else {
        this.user = new User(this.activeRouter.params['value'].id, null, null, null, null);
        this.existingUser = false;
      }
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      name: [this.user.name, Validators.required],
      address: [this.user.address, Validators.required],
      email: [this.user.email, [Validators.email, Validators.required]],
      birthdate: [this.user.birthdate, Validators.required]
    });
  }

  onSubmit() {
    this.user.name = this.form.get('name').value;
    this.user.address = this.form.get('address').value;
    this.user.birthdate = this.form.get('birthdate').value;
    this.user.email = this.form.get('email').value;

    if (this.existingUser) {
      this.userService.editUser(this.user);
    } else {
      this.userService.addUser(this.user);
    }
    this.router.navigateByUrl('/UserListComponent', {skipLocationChange: true}).then(() => this.router.navigate(['']));


  }
}
